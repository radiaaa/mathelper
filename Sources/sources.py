"""Bienvenu(e) sur le code de Mathelper, nous avons essayé de mettre un maximum de commentaires et d'espaces pour vous faciliter la lecture"""

#Bibliothèques utilisées
from tkinter import *
from tkinter import messagebox
import random


# Liste d'équations sous forme de tuples (équation, solution)
def generer_equations():
    a=random.randint(-15,15)
    b=random.randint(-15,15)
    c=random.randint(-15,15)

    while a==0 :
        a=random.randint(-15,15)
    question=f"{a}x + {b} ={c}"
    réponse= round((c-b)/a, 2)

    return (question,réponse)

# Liste d'équations du second degré sous forme de tuples (équation, solution)
equations=[generer_equations() for i in range(50)]

equations2 = [
    ("x^2 - 3x + 2 = 0", (1, 2)),
    ("x^2 - 5x + 6 = 0", (2, 3)),
    ("x^2 - 2x - 3 = 0", (-1, 3)),
    ("x^2 + 2x - 8 = 0", (-4, 2)),
]

# Liste des dérivées sous forme de tuples (fonction,dérivée)
derivees = [
    ("3x^2", "6x"),  # Dérivée de 3x^2 est 6x
    ("x^3", "3x^2"),  # Dérivée de x^3 est 3x^2
    ("4x^3 + 2x^2", "12x^2 + 4x"),  # Dérivée de 4x^3 + 2x^2 est 12x^2 + 4x
    ("sin(x)", "cos(x)"),  # Dérivée de sin(x) est cos(x)
    ("cos(x)", "-sin(x)"),  # Dérivée de cos(x) est -sin(x)
    ("e^x", "e^x"),  # Dérivée de e^x est e^x
    
]
# Liste de systèmes d'équations  sous forme de tuples  ([équation1, équation2], (solution_x, solution_y)) 
systemes_equations = [
    (["x + y = 10", "x - y = 2"], (6, 4)),  #x=6, y=4
    (["2x + 3y = 5", "4x - y = 3"], (1, 1)),  # x=1, y=1
    (["3x - 2y = 1", "x + 4y = 9"], (2, 2)),  #  x=2, y=2
    (["x + 3y = 7", "2x - y = 0"], (2, 5/3)), # x=2, y=5/3
]

 
# Fonction appelée quand l'utilisateur clique sur "Lire un cours" (fenêtre principale)
def lire_cours():
    fenetre_cours = Toplevel(window)
    fenetre_cours.title("Lire un cours")
    fenetre_cours.geometry("600x500")
    fenetre_cours.config(background="red")

    # Boutons
    bouton_equations = Button(fenetre_cours, text="Cours sur les équations",font = ("Roboto", 15), bg = "#FDFD96", fg="black", command=lambda:cours_equations())
    bouton_equations.pack(pady=20)
    button_equation2 = Button(fenetre_cours, text = "Cours sur les équations du second degré",font = ("Roboto", 15), bg = "#FDFD96", fg="black", command=lambda:cours_equations_2nd_degre())
    button_equation2.pack(pady=20)
    button_dérivée = Button(fenetre_cours, text = "Cours sur les dérivées",font = ("Roboto", 15), bg = "#FDFD96", fg="black", command=lambda:cours_derivées())
    button_dérivée.pack(pady=20)
    button_systèmeéquation = Button(fenetre_cours, text = "Cours sur les systèmes d'équations ",font = ("Roboto", 15), bg = "#FDFD96", fg="black", command=lambda:cours_systeme_equations())
    button_systèmeéquation.pack(pady=20)

# Fonction appelée quand l'utilisateur clique sur "Lire un cours"/"Cours sur les équations" (fenêtre cours)
def cours_equations():
    fenetre_cours = Toplevel(window)
    fenetre_cours.title("Cours Equations")
    fenetre_cours.geometry("600x500")
    fenetre_cours.config(background="red")

    # Contenu du cours
    contenu_cours = """
    Les équations sont des expressions mathématiques qui décrivent \n des relations entre des variables.
    Elles sont souvent utilisées pour résoudre des problèmes où une \n inconnue doit être déterminée.
    Il existe différents types d'équations, tels que les équations linéaires,\n quadratiques, polynomiales, etc.
    La résolution d'une équation consiste à trouver les valeurs \n des variables qui satisfont l'équation.

    \nBienvenue dans le cours sur les équations linéaires !
Les équations linéaires sont des équations de la forme\n ax + b = c, où a, b, et c sont des nombres et x est la variable que nous cherchons à résoudre.\n

    Résoudre une équation linéaire (ax + b = c) consiste à trouver \n la valeur de x qui rend l'équation vraie.
Pour ce faire, on isole x en effectuant des opérations algébriques simples. \n Voici les étapes :
1. Soustraire b de chaque côté de l'équation : ax = c - b
2. Diviser chaque côté de l'équation par a (a ≠ 0) : x = (c - b) / a\n
    """

    # Affichage du contenu du cours
    label_contenu = Label(fenetre_cours, text=contenu_cours, font=("Roboto", 18), bg="red", fg="white")
    label_contenu.pack(padx=20, pady=20)

# Fonction appelée quand l'utilisateur clique sur "Lire un cours"/"Cours sur les équations du second degré" (fenêtre cours)
def cours_equations_2nd_degre():
    fenetre_cours = Toplevel(window)
    fenetre_cours.title("Cours Équations du Second Degré")
    fenetre_cours.geometry("600x500")
    fenetre_cours.config(background="red")

    # Contenu du cours
    contenu_cours = """
Polynômes du Second Degré

1. Définition
Un polynôme du second degré est une expression de la forme \n ax^2 + bx + c, où a, b, et c sont des coefficients réels, avec a ≠ 0. \n Ces fonctions sont définies sur l'ensemble des nombres réels ℝ et sont \n représentées graphiquement par une parabole.

2. Forme Canonique
La forme canonique d'un polynôme du second degré est donnée par : f(x) = a(x - α)^2 + β, où α = -b/2a et β = f(α). \n Cette forme révèle le sommet de la parabole représentant la fonction.

3. Discriminant
Le discriminant du trinôme, noté Δ, est calculé par la formule Δ = b^2 - 4ac. \n Il détermine le nombre et la nature des racines de l'équation ax^2 + bx + c = 0.

4. Résolution d'Équations
Pour résoudre l'équation ax^2 + bx + c = 0...

5. Variations et Représentation Graphique
La parabole représentant f(x) = ax^2 + bx + c a son sommet en (α, β) et est symétrique par rapport à la droite x = α...
"""


    # Affichage du contenu du cours
    label_contenu = Label(fenetre_cours, text=contenu_cours, font=("Roboto", 18), bg="red", fg="white")
    label_contenu.pack(padx=20, pady=20)

# Fonction appelée quand l'utilisateur clique sur "Lire un cours"/"Cours sur les dérivées" (fenêtre cours)
def cours_derivées():
    fenetre_cours = Toplevel(window)
    fenetre_cours.title("Cours Dérivées")
    fenetre_cours.geometry("600x500")
    fenetre_cours.config(background="red")

    # Contenu du cours
    contenu_cours = """
    Cours sur les Dérivées

Les dérivées sont des concepts fondamentaux du calcul différentiel en mathématiques.
    Une dérivée représente le taux de variation instantané d'une fonction par \n rapport à sa variable indépendante.
    Elle mesure à quel point la fonction change à un point donné de son domaine.
    Les dérivées sont utilisées pour résoudre une variété de problèmes, \n tels que la détermination des pentes de courbes, la maximisation ou la minimisation de fonctions, etc.
    La notation mathématique standard pour une dérivée est f'(x) ou dy/dx, \n où f(x) est la fonction par rapport à laquelle on dérive.

"""
    # Affichage du contenu du cours
    label_contenu = Label(fenetre_cours, text=contenu_cours, font=("Roboto", 18), bg="red", fg="white")
    label_contenu.pack(padx=20, pady=20)

# Fonction appelée quand l'utilisateur clique sur "Lire un cours"/"Cours sur les systèmes d'équations" (fenêtre cours)
def cours_systeme_equations():
    fenetre_cours = Toplevel(window)
    fenetre_cours.title("Cours Systèmes d'Équations")
    fenetre_cours.geometry("600x500")
    fenetre_cours.config(background="red")

    # Contenu du cours
    contenu_cours = """
    Un système d'équations est un ensemble d'équations \n simultanées impliquant plusieurs inconnues.
    Ces équations doivent être satisfaites en même temps pour trouver les valeurs des inconnues.
    Les systèmes d'équations peuvent être linéaires ou non linéaires.
    La résolution d'un système d'équations peut se faire par différentes méthodes,\n telles que la substitution, l'élimination ou la méthode matricielle.
    Les systèmes d'équations sont couramment utilisés pour modéliser des situations \n où plusieurs conditions doivent être remplies en même temps.
    """

    # Affichage du contenu du cours
    label_contenu = Label(fenetre_cours, text=contenu_cours, font=("Roboto", 18), bg="red", fg="white")
    label_contenu.pack(padx=20, pady=20)

# Fonction appelée quand l'utilisateur clique sur "Faire des exercices" (fenêtre principale)
def ouvrir_fenetre_exercices(): 
    fenetre_exercices = Toplevel(window)
    fenetre_exercices.title("Faire des exercices")
    fenetre_exercices.geometry("600x500")
    fenetre_exercices.config(background="red")

    # Boutons 
    bouton_equations = Button(fenetre_exercices, text="S'entraîner sur des équations",font = ("Roboto", 15), bg = "#FDFD96", fg="black", command=lambda:s_entrainer_sur_equations())
    bouton_equations.pack(pady=20)
    button_equation2 = Button(fenetre_exercices, text = "S'entraîner sur des équations du second degré",font = ("Roboto", 15), bg = "#FDFD96", fg="black",command=lambda:s_entrainer_sur_equations2())
    button_equation2.pack(pady=20)
    button_dérivée = Button(fenetre_exercices, text = "S'entraîner sur des dérivées",font = ("Roboto", 15), bg = "#FDFD96", fg="black",command=lambda:s_entrainer_sur_derivee())
    button_dérivée.pack(pady=20)
    button_systèmeéquation = Button(fenetre_exercices, text = "S'entraîner sur des systèmes d'équations ",font = ("Roboto", 15), bg = "#FDFD96", fg="black",command=lambda:s_entrainer_sur_systeme())
    button_systèmeéquation.pack(pady=20)

# Fonction appelée quand l'utilisateur clique sur "Faire des exercices"/"S'entraîner sur des équations" (fenêtre exercices)
def s_entrainer_sur_equations():
    fenetre_equation = Toplevel()
    fenetre_equation.title("S'entraîner aux équations")
    fenetre_equation.geometry("400x200")

    # Définition initiale de 'equation' et 'solution' et appel du tuple
    equation, solution = random.choice(equations)

    label_equation = Label(fenetre_equation, text=f"Résoudre l'équation: {equation}", font=("Helvetica", 16))
    label_equation.pack(pady=20)

    entry_reponse = Entry(fenetre_equation, font=('Helvetica', 14), width=10)
    entry_reponse.pack(pady=10)
    
    #Vérification de la réponse de l'utilisateur
    def verifier_reponse(equation_solution):
        try:
            reponse_utilisateur = float(entry_reponse.get())
            if reponse_utilisateur == equation_solution[1]:  # Utilisation de la solution passée en paramètre
                messagebox.showinfo("Résultat", "Bravo! Bonne réponse !")
            else:
                messagebox.showerror("Résultat", f"Dommage! Mauvaise réponse. La bonne réponse est {equation_solution[1]}.")
            # passage à une nouvelle question
            nouvelle_equation, nouvelle_solution = random.choice(equations)
            label_equation.config(text=f"Résoudre l'équation: {nouvelle_equation}")
            entry_reponse.delete(0, END)
            # Mise à jour de  'verifier_reponse' avec la nouvelle solution
            bouton_verifier.config(command=lambda: verifier_reponse((nouvelle_equation, nouvelle_solution)))
        except ValueError:
            #message d'erreur quand la réponse n'est pas valide
            messagebox.showwarning("Attention", "Veuillez entrer un nombre valide.")

    bouton_verifier = Button(fenetre_equation, text="Vérifier", font=("Helvetica", 14), command=lambda: verifier_reponse((equation, solution)))
    bouton_verifier.pack(pady=20)

# Fonction appelée quand l'utilisateur clique sur "Faire des exercices"/"S'entraîner sur des équations du second degré" (fenêtre exercices)
def s_entrainer_sur_equations2():
    fenetre_equation2 = Toplevel()
    fenetre_equation2.title("S'entraîner aux équations du second degré")
    fenetre_equation2.geometry("500x300")

    equation2, (solution1, solution2) = random.choice(equations2)

    label_equation2 = Label(fenetre_equation2, text=f"Résoudre l'équation du second degré: {equation2}", font=("Helvetica", 16))
    label_equation2.pack(pady=20)

    entry_reponse1 = Entry(fenetre_equation2, font=('Helvetica', 14), width=10)
    entry_reponse1.pack(pady=10)
    entry_reponse2 = Entry(fenetre_equation2, font=('Helvetica', 14), width=10)
    entry_reponse2.pack(pady=10)

    def verifier_reponse():
        nonlocal solution1, solution2
        try:
            reponse_utilisateur1 = float(entry_reponse1.get())
            reponse_utilisateur2 = float(entry_reponse2.get())
            if {reponse_utilisateur1, reponse_utilisateur2} == {solution1, solution2}:
                messagebox.showinfo("Résultat", "Bravo! Bonne réponse !")
            else:
                messagebox.showerror("Résultat", f"Dommage! Mauvaise réponse. Les bonnes réponses sont {solution1} et {solution2}.")
        except ValueError:
            messagebox.showwarning("Attention", "Veuillez entrer des nombres valides.")

        # Mise à jour pour une nouvelle question
        nouveau_systeme, (nouveau_sol_x, nouveau_sol_y) = random.choice(equations2)
        label_equation2.config(text=f"Résoudre l'équation du second degré: {nouveau_systeme}")
        entry_reponse1.delete(0, 'end')
        entry_reponse2.delete(0, 'end')

        # Mise à jour des solutions pour la nouvelle question
        solution1, solution2 = nouveau_sol_x, nouveau_sol_y

    bouton_verifier = Button(fenetre_equation2, text="Vérifier", font=("Helvetica", 14), command=verifier_reponse)
    bouton_verifier.pack(pady=20)

# Fonction appelée quand l'utilisateur clique sur "Faire des exercices"/"S'entraîner sur des dérivées" (fenêtre exercices)
def s_entrainer_sur_derivee():
    fenetre_derivee = Toplevel()
    fenetre_derivee.title("S'entraîner sur des dérivées")
    fenetre_derivee.geometry("400x200")

    # Sélection initiale d'une fonction et sa dérivée
    fonction, derivee = random.choice(derivees)

    label_fonction = Label(fenetre_derivee, text=f"Trouver la dérivée de : {fonction}", font=("Helvetica", 16))
    label_fonction.pack(pady=20)

    entry_reponse = Entry(fenetre_derivee, font=('Helvetica', 14), width=10)
    entry_reponse.pack(pady=10)

    def verifier_reponse(fonction_derivee):
        reponse_utilisateur = entry_reponse.get().strip()  # strip() pour enlever les espaces inutiles
        if reponse_utilisateur == fonction_derivee[1]:  # Utilisation de la dérivée passée en paramètre
            messagebox.showinfo("Résultat", "Bravo! Bonne réponse !")
        else:
            messagebox.showerror("Résultat", f"Dommage! Mauvaise réponse. La bonne réponse est {fonction_derivee[1]}.")

        # Mise à jour pour une nouvelle question
        nouvelle_fonction, nouvelle_derivee = random.choice(derivees)
        label_fonction.config(text=f"Trouver la dérivée de : {nouvelle_fonction}")
        entry_reponse.delete(0, 'end')
        # Mise à jour de la commande du bouton avec la nouvelle fonction et dérivée
        bouton_verifier.config(command=lambda: verifier_reponse((nouvelle_fonction, nouvelle_derivee)))

    bouton_verifier = Button(fenetre_derivee, text="Vérifier", font=("Helvetica", 14), command=lambda: verifier_reponse((fonction, derivee)))
    bouton_verifier.pack(pady=20)


# Fonction appelée quand l'utilisateur clique sur "Faire des exercices"/"S'entraîner sur des systèmes d'équations" (fenêtre exercices)
def s_entrainer_sur_systeme():
    fenetre_systeme = Toplevel()
    fenetre_systeme.title("S'entraîner sur des systèmes d'équations")
    fenetre_systeme.geometry("500x300")

    systeme, (solution_x, solution_y) = random.choice(systemes_equations)

    label_systeme = Label(fenetre_systeme, text=f"Résoudre le système d'équations :\n{systeme[0]}\n{systeme[1]}", font=("Helvetica", 16))
    label_systeme.pack(pady=20)

    label_x = Label(fenetre_systeme, text="Solution pour x :", font=("Helvetica", 14))
    label_x.pack(pady=5)
    entry_reponse_x = Entry(fenetre_systeme, font=('Helvetica', 14), width=10)
    entry_reponse_x.pack(pady=5)
    label_y = Label(fenetre_systeme, text="Solution pour y :", font=("Helvetica", 14))
    label_y.pack(pady=5)
    entry_reponse_y = Entry(fenetre_systeme, font=('Helvetica', 14), width=10)
    entry_reponse_y.pack(pady=5)

    def verifier_reponse(reponse_x, reponse_y, sol_x, sol_y):
        try:
            if float(reponse_x.get()) == sol_x and float(reponse_y.get()) == sol_y:
                messagebox.showinfo("Résultat", "Bravo! Bonne réponse !")
            else:
                messagebox.showerror("Résultat", f"Dommage! Mauvaise réponse. La bonne réponse est x={sol_x}, y={sol_y}.")
        except ValueError:
            messagebox.showwarning("Attention", "Veuillez entrer des nombres valides pour x et y.")
        
        #  une nouvelle question
        nouveau_systeme, (nouveau_sol_x, nouveau_sol_y) = random.choice(systemes_equations)
        label_systeme.config(text=f"Résoudre le système d'équations :\n{nouveau_systeme[0]}\n{nouveau_systeme[1]}")
        entry_reponse_x.delete(0, 'end')
        entry_reponse_y.delete(0, 'end')
        bouton_verifier.config(command=lambda: verifier_reponse(entry_reponse_x, entry_reponse_y, nouveau_sol_x, nouveau_sol_y))

    bouton_verifier = Button(fenetre_systeme, text="Vérifier", font=("Helvetica", 14), height=20, command=lambda: verifier_reponse(entry_reponse_x, entry_reponse_y, solution_x, solution_y))
    bouton_verifier.pack(pady=20)


#creer une fenetre principale
window = Tk()

#personnaliser la fenetre
window.title("Mathelper")
window.geometry("1150x600")
window.minsize(480,360) #la taille minimal de la fenetre "window"
window.config(background="red") #couleur de l'arriere plan

#creer la frame
frame = Frame(window, bg="red") #creation d'une frame pour centrer les elements

#ajouter un premier texte
label_title = Label(frame, text="Bienvenu(e) sur Mathelper !", font = ("Roboto", 30), bg = "red", fg="white")
label_title.pack(pady=50)

#ajouter un deuxieme texte
label_question = Label(frame, text="Que souhaitez-vous faire ?", font = ("Roboto", 25), bg = "red", fg="white")
label_question.pack(pady=50)

#ajouter un premier bouton
button_cours = Button(frame, text = "Lire un cours",font = ("Roboto", 15), bg = "#FDFD96", fg="black",command=lambda:lire_cours())#"lambda" permet d'executer la fonction quand le bouton est presse et pas avant
button_cours.pack(pady=10)
button_exercices = Button(frame, text = "Faire des exercices",font = ("Roboto", 15), bg = "#FDFD96", fg="black", command=lambda:ouvrir_fenetre_exercices()) #"lambda" permet d'executer la fonction quand le bouton est presse et pas avant
button_exercices.pack(pady=10)

#ajouter un troisieme texte
label_noms = Label(frame, text="Projet réalisé dans le cadre des trophées NSI 2024", font = ("Roboto", 15), bg = "red", fg="white")
label_noms.pack(pady=50)

#ajouter la frame
frame.pack(expand=YES) #texte sera toujours au centre

#afficher fenetre
window.mainloop()