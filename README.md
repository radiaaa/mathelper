
# Mathelper

Utilisation de l'interface

Une fois avoir lancé l'application, une fenetre menu s'affiche avec 2 boutons : 
 Le premier permet de reviser ses cours avec un bouton "cours". Lorsque celui-ci est cliqué une nouvelle fenetre s'affiche avec 4 boutons : "cours sur le second degré", "cours sur les équations", "cours sur les derivees" et "cours sur les systemes d'equation". Cliquer sur un des boutons va afficher le cours de la leçon, permettant ainsi à l'utilisateur de réviser ses cours. 

 Le second bouton du menu correspond au bouton "exercices". Lorsque celui-ci est cliqué, cela va afficher une nouvelle fenetre avec 4 boutons également : "s'entrainer sur les dérivées", "s'entrainer sur les équations de second degré", "s'entrainer sur les systèmes d'equation", et "s'entrainer sur les equations". Une fois qu´un de ces boutons est cliqué, la question en rapport avec le chapitre va être générée et affichée avec une zone de texte dans laquelle l'utilisateur doit repondre puis verifier sa réponse en cliquant sur le bouton vérifier. Le bouton vérifier va lancer une fenêtre qui affichera, selon la réponse de l'utilisateur, si la réponse est correcte ou non.




## Bibliothèque à importer

Tkinter ;
Random (déjà installée)




## Installation

Installer Tkinter

Tkinter est installé par défaut, si ce n'est pas le cas, lancez la commande suivante:

sudo apt-get install python-tk

En python 3:

sudo apt-get install python3-tk
Python 2, python 3

Les modules ne sont pas les mêmes suivant votre version de python. Si le message suivant apparaît lors de l'exécution de votre script:

ImportError: No module named 'Tkinter'
C'est que le module appelé n'est pas le bon par rapport à votre version python.

Python 2             Python 3
Tkinter         →  tkinter
Tix             →  tkinter.tix
ttk             →  tkinter.ttk
tkMessageBox    →  tkinter.messagebox
tkColorChooser  →  tkinter.colorchooser
tkFileDialog    →  tkinter.filedialog
tkCommonDialog  →  tkinter.commondialog
tkSimpleDialog  →  tkinter.simpledialog
tkFont          →  tkinter.font
Tkdnd           →  tkinter.dnd
ScrolledText    →  tkinter.scrolledtext


## Documentation

[Installation de l'interface graphique Tkinter](https://python.doctor/page-tkinter-interface-graphique-python-tutoriel)
[Apprendre a utiliser l'interface Tkinter](https://docs.python.org/3/library/tk.html)
